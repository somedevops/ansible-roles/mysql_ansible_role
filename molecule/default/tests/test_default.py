"""Role testing files using testinfra."""

def test_mysql_package(host):
    pkg = host.package("mysql-server")
    assert pkg.is_installed

def test_mysql_service(host):
    s = host.service("mysql")
    assert s.is_running
    assert s.is_enabled

def test_mysql_users_exists(host):
    existing_users = host.check_output("mysql -uroot -ppassword -e 'SELECT User FROM mysql.user'")
    assert 'user1' in existing_users
    assert 'user2' in existing_users

def test_mysql_database_exists(host):
    existing_databases = host.check_output("mysql -uroot -ppassword -e 'SHOW DATABASES;'")
    assert 'test_db1' in existing_databases
    assert 'test_db2' in existing_databases
